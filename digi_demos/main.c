//Copyright (C) 2018 Brundo Salvatore
//salvo85[at]gmail[dot]com
//http://www.brundo.org

#include <avr/io.h>
#define F_CPU 16500000 // 16.5 Mhz is default frequency clock of a digispark!
#include <util/delay.h>
#include "sw_rgb_pwm.h"

int main(){
  init_rgb_led();
  while(1){
    blink();
    glow();
    rgb_space();
    // marrone
    // rgb_colour(20, 5, 1);
    // rgb_colour(250,10,1);
  }
}
