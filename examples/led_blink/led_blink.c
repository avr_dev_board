#include <avr/io.h>
#include <util/delay.h>

void delayms(uint16_t millis)
{
	while (millis) {
		_delay_ms(1);
		millis--;
	}
}

int main(void)
{
	DDRD |= 1 << DDD1;			// set LED pin PD1 to output
	while (1) {
		PORTD &= ~(1 << PORTD1);	// LED off
		delayms(500);			// delay 900 ms
		PORTD |= 1 << PORTD1; 		// LED on
		delayms(500);			// delay 100 ms
	}

	return 0;
}
