MCU	= atmega328
F_CPU	= 16000000UL

CC	= avr-gcc
OBJCOPY = avr-objcopy
OBJDUMP = avr-objdump
AVRSIZE = avr-size
AVRDUDE = avrdude

PROGRAMMER	= avrisp2
PROGRAMMER_DEV	= usb

TARGET	= led_blink

CFLAGS	= -Os -DF_CPU=$(F_CPU)
ARCH	= -mmcu=$(MCU)

%.elf: %.c
	$(CC) $(CFLAGS) $(ARCH) -o $@ $<

%.hex: %.elf
	$(OBJCOPY) -O ihex $< $@

all: $(TARGET).hex

flash: $(TARGET).hex
	$(AVRDUDE) -c $(PROGRAMMER) -P $(PROGRAMMER_DEV) -p $(MCU) -U flash:w:$<

# there's an issue with the efuse
LFUSE = 0xff
HFUSE = 0xde
EFUSE = 0x05

FUSE_STRING = -U lfuse:w:$(LFUSE):m -U hfuse:w:$(HFUSE):m -U efuse:w:$(EFUSE):m 

read_fuses:
	$(AVRDUDE) -c $(PROGRAMMER) -p $(MCU) -nv	

fuses:
	$(AVRDUDE) -c $(PROGRAMMER) -P $(PROGRAMMER_DEV) -p $(MCU) $(FUSE_STRING)

clean:
	rm -f *.hex *.o *.elf

.PHONY: all clean flash fuses read_fuses
